# Criando um Pipeline de Deploy com GitLab e Kubernetes - desafio
Repositório com o código do desafio da aula "Criando um Pipeline de Deploy com GitLab e Kubernetes". Bootcamp Jornada DevOps com AWS - Impulso, DIO. 

## O que foi desenvolvido nesse projeto?
Foi feito um pipeline de deploy de imagens Docker e criação de deployments em um cluster kubernetes em nuvem com o GCP.

> Link do bootcamp [AQUI](https://web.dio.me/track/jornada-devops-com-aws-impulso)!

![Logo do bootcamp](https://hermes.digitalinnovation.one/tracks/7b035b91-8625-493c-a816-6740a4a25e9b.png)